# Laravel Permission Seeder Tempalate

## Table of Contents

1.  [Installation](#installation)
2.  [Publish Seeder](#Publish Seeder)
3.  [Custom Config](#Custom Config)
    
## Installation

```terminal
composer require taxibonbon/laravel-permission-seeder-template
``` 

## Seeder Permission

Publish Seeder

```terminal
php artisan vendor:publish --tag=laravel_permission_seeder
```

## Custom Config

```php
// config/permission_template_seeder.php

return [
    'module_name' => 'User',
    'model_path' => 'Entities/User.php'
];
```
