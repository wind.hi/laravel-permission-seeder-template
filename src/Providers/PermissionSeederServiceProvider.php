<?php

namespace Taxibonbon\LaravelPermissionSeederTemplate\Providers;

use Illuminate\Support\ServiceProvider;

class PermissionSeederServiceProvider extends ServiceProvider
{
    public function boot()
    {
        $this->publishes([
            __DIR__ . '/../Database/Seeders/LaravelUserTemplateSeeder.php' => database_path('seeders/LaravelUserTemplateSeeder.php'),
            __DIR__ . '/../Database/Seeders/LaravelPermissionTemplateSeeder.php' => database_path('seeders/LaravelPermissionTemplateSeeder.php'),
            __DIR__ . '/../Models/User.php' => module_path('User', 'Entities/User.php'),
            __DIR__ . '/../Config/permission_template_seeder.php' => config_path('permission_template_seeder.php'),
        ], 'laravel_permission_seeder');
    }

    public function register()
    {

    }
}

