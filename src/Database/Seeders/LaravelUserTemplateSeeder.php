<?php

namespace Database\Seeders;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Modules\User\Entities\User;
use Database\Seeders\LaravelPermissionTemplateSeeder;

class LaravelUserTemplateSeeder extends Seeder
{
    public function run()
    {
        Model::unguard();

        Schema::disableForeignKeyConstraints();

        DB::table('roles')->truncate();
        DB::table('users')->truncate();
        DB::table('role_has_permissions')->truncate();
        DB::table('model_has_roles')->truncate();
        DB::table('permissions')->truncate();

        $this->call(LaravelPermissionTemplateSeeder::class);

        /**
         * create default user
         * @var User $user
         */
        $user = User::query()->create([
            'name' => 'admin',
            'email' => 'admin@admin.com',
            'password' => bcrypt('123qaz')
        ]);

        $user->assignRole('admin');
    }

}
