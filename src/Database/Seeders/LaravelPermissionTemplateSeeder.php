<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class LaravelPermissionTemplateSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $permissions = [
            'users.browse' => 'admin',
            'users.create' => 'admin',
            'users.update' => 'admin',
            'users.destroy' => 'admin',
        ];

        foreach ($permissions as $key => $value) {
            Permission::query()->updateOrCreate(
                ['name' => $key],
                ['guard_name' => $value]
            );
        }

        // crate default role admin
        $role = Role::create(['name' => 'admin', 'guard_name' => 'admin']);

        $role->givePermissionTo(Permission::all());
    }
}
